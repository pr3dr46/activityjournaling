﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity.Owin;

namespace ActivityJournaling.Web.Controllers
{
    public class BaseController : ApiController
    {
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                if (_userManager == null)
                    _userManager = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
                return _userManager;
            }
        }
    }
}
