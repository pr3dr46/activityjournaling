﻿using ActivityJournaling.DataLayer;
using ActivityJournaling.Services.Core;
using ActivityJournaling.Services.Models;
using ActivityJournaling.WebApi.Helpers;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace ActivityJournaling.WebApi.Controllers
{
    [Authorize]
    [RoutePrefix("api/Data")]
    public class DataController : ApiController
    {
        //api/Task/GetTasks
        [HttpPost]
        [Route("SaveEntry")]
        public JsonResponse SaveEntry(int? id, string subject, string tags, string entry, DateTime? date, DataType type)
        {
            try
            {
                var ids = new List<int>();
                var tagDtos = new List<TagDto>();
                if (!string.IsNullOrEmpty(tags))
                {
                    ids = tags.Split(',').Select(x => int.Parse(x)).ToList();
                    foreach (var tagId in ids)
                        tagDtos.Add(new TagDto() { Id = tagId });
                }

                DataService.SaveEntry(User.Identity.GetUserId<int>(), id, subject, tagDtos, entry, date, type);
                return ResponseHelper.GetJsonResponse(true);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return ResponseHelper.GetErrorJsonResponse(ex.Message);
            }
        }

        //api/Task/DeleteEntry
        [HttpPost]
        [Route("DeleteEntry")]
        public JsonResponse DeleteEntry(int id, DataType type)
        {
            try
            {
                DataService.DeleteEntry(id, type);
                return ResponseHelper.GetJsonResponse(true);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return ResponseHelper.GetErrorJsonResponse(ex.Message);
            }
        }

        [HttpPost]
        [Route("GetEntries")]
        public JsonResponse GetEntries(DateTime date, DataType type)
        {
            try
            {
                var data = DataService.GetEntries(User.Identity.GetUserId<int>(), date, type);
                return ResponseHelper.GetJsonResponse(data);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return ResponseHelper.GetErrorJsonResponse(ex.Message);
            }
        }

        [HttpPost]
        [Route("GetEntry")]
        public JsonResponse GetEntry(int? id, DataType type)
        {
            try
            {
                var data = DataService.GetEntry(id, type);

                return ResponseHelper.GetJsonResponse(data);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return ResponseHelper.GetErrorJsonResponse(ex.Message);
            }
        }

        [HttpPost]
        [Route("GetDataStatus")]
        public JsonResponse GetDataStatus()
        {
            try
            {
                var data = DataService.GetDataStatus(User.Identity.GetUserId<int>());

                return ResponseHelper.GetJsonResponse(data);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return ResponseHelper.GetErrorJsonResponse(ex.Message);
            }
        }
    }
}
