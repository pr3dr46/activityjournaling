﻿using ActivityJournaling.DataLayer;
using ActivityJournaling.Services.Core;
using ActivityJournaling.Services.Models;
using ActivityJournaling.WebApi.Helpers;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace ActivityJournaling.WebApi.Controllers
{
    [Authorize]
    [RoutePrefix("api/Task")]
    public class TaskController : ApiController
    {
        //api/Task/SaveTask
        [HttpPost]
        [Route("SaveTask")]
        public JsonResponse SaveTask(int? id, string subject, string description, DateTime? date,
            string tags, string comment, int? emojiState, string calendarEventIdentifier)
        {
            try
            {
                //populate data 
                var task = new TaskDto();
                task.Id = id;
                task.Subject = subject;
                task.Description = description;
                task.TaskDate = date;
                task.UserId = User.Identity.GetUserId<int>();
                task.Comment = comment;
                task.EmojiState = emojiState;
                task.CalendarEventIdentifier = calendarEventIdentifier;
                var ids = new List<int>();
                if (!string.IsNullOrEmpty(tags))
                {
                    ids = tags.Split(',').Select(x => int.Parse(x)).ToList();
                    task.Tags = new List<TagDto>();
                    foreach (var tagId in ids)
                        task.Tags.Add(new TagDto() { Id = tagId });
                }

                var taskId = TaskService.SaveTask(task);
                return ResponseHelper.GetJsonResponse(taskId);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return ResponseHelper.GetErrorJsonResponse(ex.Message);
            }
        }

        //api/Task/GetTasks
        [HttpPost]
        [Route("GetTasks")]
        public JsonResponse GetTasks(DateTime date, TaskStatuses status)
        {
            try
            {
                var data = TaskService.GetTasks(User.Identity.GetUserId<int>(), date, status);
                return ResponseHelper.GetJsonResponse(data);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return ResponseHelper.GetErrorJsonResponse(ex.Message);
            }
        }

        //api/Task/GetTasks
        [HttpPost]
        [Route("GetTask")]
        public JsonResponse GetTask(int? id)
        {
            try
            {
                var data = TaskService.GetTask(User.Identity.GetUserId<int>(), id);
                return ResponseHelper.GetJsonResponse(data);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return ResponseHelper.GetErrorJsonResponse(ex.Message);
            }
        }

        [HttpPost]
        [Route("DeleteTask")]
        public JsonResponse DeleteTask(int? id)
        {
            try
            {
                TaskService.DeleteTask(id);
                return ResponseHelper.GetJsonResponse(string.Empty);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return ResponseHelper.GetErrorJsonResponse(ex.Message);
            }
        }

        //api/Task/UpdateStatus
        [HttpPost]
        [Route("UpdateStatus")]
        public JsonResponse UpdateStatus(int? id, TaskStatuses status)
        {
            try
            {
                TaskService.UpdateStatus(id, status);
                return ResponseHelper.GetJsonResponse(string.Empty);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return ResponseHelper.GetErrorJsonResponse(ex.Message);
            }
        }

        [HttpPost]
        [Route("GetTasksStatus")]
        public JsonResponse GetTasksStatus()
        {
            try
            {
                var data = TaskService.GetTaskStatus(User.Identity.GetUserId<int>());
                return ResponseHelper.GetJsonResponse(data);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return ResponseHelper.GetErrorJsonResponse(ex.Message);
            }
        }
    }
}
