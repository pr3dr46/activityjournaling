﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ActivityJournaling.WebApi.Helpers
{
    public static class ResponseHelper
    {
        public static JsonResponse GetJsonResponse<T>(T data)
        {
            return new JsonResponse()
            {
                Status = ResponseStatuses.OK,
                Message = "",
                Data = data
            };
        }

        public static JsonResponse GetJsonResponse<T>(T data, string message)
        {
            return new JsonResponse()
            {
                Status = ResponseStatuses.OK,
                Message = message,
                Data = data
            };
        }

        public static JsonResponse GetErrorJsonResponse(string message)
        {
            return new JsonResponse()
            {
                Status = ResponseStatuses.ERROR,
                Message = message,
                Data = null
            };
        }

        public static JsonResponse GetErrorJsonResponse<T>(T data, string message)
        {
            return new JsonResponse()
            {
                Status = ResponseStatuses.ERROR,
                Message = message,
                Data = data
            };
        }
    }

    public class JsonResponse
    {
        public string Status { get; set; }

        public string Message { get; set; }

        public object Data { get; set; }
    }

    public class JsonResponse<T>
    {
        public string Status { get; set; }

        public string Message { get; set; }

        public T Data { get; set; }
    }

    public static class ResponseStatuses
    {
        public const string OK = "ok";

        public const string ERROR = "error";
    }
}