﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ActivityJournaling.Web.Helpers
{
    public static class Constants
    {
        public static class Claims
        {
            public const string USERNAME = "username";
            public const string ID = "id";
            public const string FIRST_NAME = "firstname";
            public const string LAST_NAME = "lastname";
        }
    }
}