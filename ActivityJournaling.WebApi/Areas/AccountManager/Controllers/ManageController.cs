﻿using ActivityJournaling.Web;
using ActivityJournaling.WebApi.Areas.AccountManager.Models;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using ActivityJournaling.Services.Core;
using ActivityJournaling.WebApi.Helpers;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ActivityJournaling.WebApi.Areas.AccountManager.Controllers
{
    public class ManageController : Controller
    {
        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                if(_userManager == null)
                {
                    _userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    var provider = new DpapiDataProtectionProvider("ActivityJournaling");
                    UserManager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser, int>(provider.Create("UserToken"))
                        as IUserTokenProvider<ApplicationUser, int>;
                }

                return _userManager;
            }
            private set
            {
                _userManager = value;
            }
        }

        public ManageController()
        {
          
        }

        // GET: AccountManager/Manage
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ResetPassword(string token)
        {
            //var decoded = HttpUtility.UrlDecode(token);

            var email = EncryptionManager.DecryptRijndael(token);

            var user = UserManager.FindByEmail(email);

            if (user == null)
                throw new Exception("Email token is not valid");

            ViewBag.Token = token;

            return View();
        }

        [HttpPost]
        public JsonResult SetPassword(SetPasswordViewModel model)
        {
            //var decoded = HttpUtility.UrlDecode(model.Token);
            var email = EncryptionManager.DecryptRijndael(model.Token);

            var user = UserManager.FindByEmail(email);

            if (user == null)
                return Json(new { status = "error", message = "Email token is not valid" });

            var code = UserManager.GeneratePasswordResetToken(user.Id);
            var result = UserManager.ResetPassword(user.Id, code, model.Password);

            if (!result.Succeeded)
                return Json(new { status = "error", message = string.Join(", ", result.Errors) });

            return Json(new { status = "ok" });
        }
    }
}