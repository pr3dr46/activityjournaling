﻿using Elmah.Contrib.WebApi;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace ActivityJournaling.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            GlobalConfiguration.Configuration.Filters.Add(new ElmahHandleErrorApiAttribute());

        }
    }
}
