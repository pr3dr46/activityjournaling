﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActivityJournaling.DataLayer
{
    public static class TagTypes
    {
        public const string TASK = "task";

        public const string STRUGGLE = "struggle";

        public const string BETTERMENT = "betterment";

        public const string INTERESTED_READ = "interestedread";
    }

    public enum TaskStatuses
    {
        Pending = 1,
        Done
    }

    public enum DataType
    {
        Struggle = 1,
        Beterrments,
        PointOfInterest
    }
}
