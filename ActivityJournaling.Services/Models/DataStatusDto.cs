﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActivityJournaling.Services.Models
{
    public class DataStatusDto
    {
        public DataStatusDto()
        {
            this.BettermentsTags = new List<string>();
            this.StruggleTags = new List<string>();
            this.AreaOfGratitudeTags = new List<string>();
        }

        public List<string> StruggleTags { get; set; }

        public List<string> BettermentsTags { get; set; }

        public List<string> AreaOfGratitudeTags { get; set; }
    }
}
