﻿using ActivityJournaling.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActivityJournaling.Services.Models
{
    public class TaskDto
    {
        public int? Id { get; set; }

        public int? UserId { get; set; }

        public DateTime? TaskDate { get; set; }

        public TaskStatuses? Status { get; set; }

        public string Subject { get; set; }

        public string Description { get; set; }

        public string Comment { get; set; }

        public int? EmojiState { get; set; }

        public bool? SyncWithCalendar { get; set; }

        public string CalendarEventIdentifier { get; set; }

        public List<TagDto> Tags { get; set; }
    }
}
