﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActivityJournaling.Services.Models
{
    public class TagDto
    {
        public int Id { get; set; }

        public string TagName { get; set; }

        public string TagType { get; set; }

        public bool IsSelected { get; set; }
    }
}
