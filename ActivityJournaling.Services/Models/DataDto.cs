﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActivityJournaling.Services.Models
{
    public class DataDto
    {
        public int? Id { get; set; }

        public string Subject { get; set; }

        public List<TagDto> Tags { get; set; }

        public string Entry { get; set; }

        public DateTime? Date { get; set; }
    }
}
