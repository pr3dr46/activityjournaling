﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActivityJournaling.Services.Models
{
    public class TaskCountStatusDto
    {
        public int CompletedTasks { get; set; }

        public int PendingTasks { get; set; }

        public int Status { get; set; } //1 - good, 4 bad
    }
}
