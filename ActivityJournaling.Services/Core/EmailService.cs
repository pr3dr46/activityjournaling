﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace ActivityJournaling.Services.Core
{
    public class EmailService
    {
        public static void SendEmail(string to, string subject, string body)
        {
            var userName = ConfigurationManager.AppSettings["SendGridUser"];
            var password = ConfigurationManager.AppSettings["SendGridPassword"];
            var apiKey = ConfigurationManager.AppSettings["SendGridKey"];

            MailMessage mailMsg = new MailMessage();

            // To
            mailMsg.To.Add(new MailAddress(to, to));

            // From
            mailMsg.From = new MailAddress("predrag@devops-x.com", "DigiDocs");

            // Subject and multipart/alternative Body
            mailMsg.Subject = subject;
            string html = body;
            mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html));

            // Init SmtpClient and send
            using (SmtpClient smtpClient = new SmtpClient("smtp.sendgrid.net", Convert.ToInt32(587)))
            {
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(userName, password);
                smtpClient.Credentials = credentials;

                smtpClient.Send(mailMsg);
            }
        }

        public static SmtpClient GetConnection()
        {
            var server = ConfigurationManager.AppSettings["smtpServer"];
            var port = Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"]);
            var userName = ConfigurationManager.AppSettings["smtpUsername"];
            var password = ConfigurationManager.AppSettings["smtpPassword"];
            var useSSL = ConfigurationManager.AppSettings["smtpUseSSL"] == "true";

            var smtp = new SmtpClient(server);
            smtp.Timeout = 10000;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new System.Net.NetworkCredential(userName, password);
            smtp.EnableSsl = useSSL;
            smtp.Port = port;
            return smtp;
        }
    }
}
