﻿using ActivityJournaling.DataLayer;
using ActivityJournaling.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActivityJournaling.Services.Core
{
    public class DataService
    {
        public static void SaveEntry(int userId, int? id, string subject, List<TagDto> tags, string entry, DateTime? date, DataType type)
        {
            using (var data = new ActivityJournalingEntities())
            {
                switch (type)
                {
                    case DataType.Struggle:
                        {
                            Struggle dbStruggle = new Struggle();
                            if (id > 0)
                                dbStruggle = data.Struggles.Find(id.Value);
                            else
                                data.Struggles.Add(dbStruggle);

                            dbStruggle.Description = entry;
                            dbStruggle.StruggleDate = date;
                            dbStruggle.Subject = subject;
                            dbStruggle.UserId = userId;

                            if (tags != null)
                            {
                                dbStruggle.StruggleTags.Clear();

                                foreach (var tag in tags)
                                {
                                    var dbTag = new StruggleTag();
                                    dbTag.TagId = tag.Id;
                                    dbTag.StruggleId = dbStruggle.Id;
                                    data.StruggleTags.Add(dbTag);
                                }
                            }

                            data.SaveChanges();

                            break;
                        }
                    case DataType.Beterrments:
                        {
                            Betterment dbBetterment = new Betterment();
                            if (id > 0)
                                dbBetterment = data.Betterments.Find(id.Value);
                            else
                                data.Betterments.Add(dbBetterment);

                            dbBetterment.Description = entry;
                            dbBetterment.BeterrmentDate = date;
                            dbBetterment.Subject = subject;
                            dbBetterment.UserId = userId;

                            if (tags != null)
                            {
                                dbBetterment.BettermentTags.Clear();

                                foreach (var tag in tags)
                                {
                                    var dbTag = new BettermentTag();
                                    dbTag.TagId = tag.Id;
                                    dbTag.BettermentId = dbBetterment.Id;
                                    data.BettermentTags.Add(dbTag);
                                }
                            }

                            data.SaveChanges();

                            break;
                        }
                    case DataType.PointOfInterest:
                        {
                            InterestedRead dbInterestingRead = new InterestedRead();
                            if (id > 0)
                                dbInterestingRead = data.InterestedReads.Find(id.Value);
                            else
                                data.InterestedReads.Add(dbInterestingRead);

                            dbInterestingRead.Description = entry;
                            dbInterestingRead.InterestedReadDate = date;
                            dbInterestingRead.Subject = subject;
                            dbInterestingRead.UserId = userId;

                            if (tags != null)
                            {
                                dbInterestingRead.InterestedReadTags.Clear();

                                foreach (var tag in tags)
                                {
                                    var dbTag = new InterestedReadTag();
                                    dbTag.TagId = tag.Id;
                                    dbTag.InterestedReadId = dbInterestingRead.Id;
                                    data.InterestedReadTags.Add(dbTag);
                                }
                            }

                            data.SaveChanges();

                            break;
                        }
                    default:
                        break;
                }
            }
        }

        public static List<DataDto> GetEntries(int userId, DateTime date, DataType type)
        {
            using (var data = new ActivityJournalingEntities())
            {
                switch (type)
                {
                    case DataType.Struggle:
                        {
                            var results = data.Struggles
                                .Where(x => x.StruggleDate.Value.Day == date.Day &&
                                             x.StruggleDate.Value.Month == date.Month &&
                                             x.StruggleDate.Value.Year == date.Year &&
                                             x.UserId == userId)
                                 .Select(x => new DataDto
                                 {
                                     Id = x.Id,
                                     Date = x.StruggleDate,
                                     Entry = x.Description,
                                     Subject = x.Subject,
                                     Tags = x.StruggleTags.Select(y => new TagDto()
                                     {
                                         Id = y.Id,
                                         TagName = y.Tag.TagName,
                                         TagType = y.Tag.TagType
                                     })
                                     .ToList()
                                 })
                                 .ToList();

                            return results;
                        }
                    case DataType.Beterrments:
                        {
                            var results = data.Betterments
                                    .Where(x => x.BeterrmentDate.Value.Day == date.Day &&
                                                 x.BeterrmentDate.Value.Month == date.Month &&
                                                 x.BeterrmentDate.Value.Year == date.Year &&
                                                 x.UserId == userId)
                                     .Select(x => new DataDto
                                     {
                                         Id = x.Id,
                                         Date = x.BeterrmentDate,
                                         Entry = x.Description,
                                         Subject = x.Subject,
                                         Tags = x.BettermentTags.Select(y => new TagDto()
                                         {
                                             Id = y.Id,
                                             TagName = y.Tag.TagName,
                                             TagType = y.Tag.TagType
                                         })
                                         .ToList()
                                     })
                                     .ToList();

                            return results;
                        }
                    case DataType.PointOfInterest:
                        {
                            var results = data.InterestedReads
                                    .Where(x => x.InterestedReadDate.Value.Day == date.Day &&
                                                 x.InterestedReadDate.Value.Month == date.Month &&
                                                 x.InterestedReadDate.Value.Year == date.Year &&
                                                 x.UserId == userId)
                                     .Select(x => new DataDto
                                     {
                                         Id = x.Id,
                                         Date = x.InterestedReadDate,
                                         Entry = x.Description,
                                         Subject = x.Subject,
                                         Tags = x.InterestedReadTags.Select(y => new TagDto()
                                         {
                                             Id = y.Id,
                                             TagName = y.Tag.TagName,
                                             TagType = y.Tag.TagType
                                         })
                                         .ToList()
                                     })
                                     .ToList();

                            return results;
                        }
                    default:
                        return null;
                }
            }
        }

        public static void DeleteEntry(int id, DataType type)
        {
            using (var data = new ActivityJournalingEntities())
            {
                switch (type)
                {
                    case DataType.Struggle:
                        {
                            var entry = data.Struggles.Find(id);
                            var tags = entry.StruggleTags.ToList();
                            foreach (var tag in tags)
                                entry.StruggleTags.Remove(tag);
                            data.Struggles.Remove(entry);
                            data.SaveChanges();
                            break;
                        }
                    case DataType.Beterrments:
                        {
                            var entry = data.Betterments.Find(id);
                            var tags = entry.BettermentTags.ToList();
                            foreach (var tag in tags)
                                entry.BettermentTags.Remove(tag);
                            data.Betterments.Remove(entry);
                            data.SaveChanges();
                            break;
                        }
                    case DataType.PointOfInterest:
                        {
                            var entry = data.InterestedReads.Find(id);
                            var tags = entry.InterestedReadTags.ToList();
                            foreach (var tag in tags)
                                entry.InterestedReadTags.Remove(tag);
                            data.InterestedReads.Remove(entry);
                            data.SaveChanges();
                            break;
                        }
                    default:
                        break;
                }
            }
        }

        public static DataDto GetEntry(int? id, DataType type)
        {
            using (var data = new ActivityJournalingEntities())
            {
                var model = new DataDto();
                switch (type)
                {
                    case DataType.Struggle:
                        {
                            var entry = data.Struggles.Find(id);
                            if (entry == null)
                                return new DataDto()
                                {
                                    Tags = data.Tags.Select(x => new TagDto() { Id = x.Id, TagName = x.TagName, TagType = x.TagType }).ToList()
                                };

                            model.Id = entry.Id;
                            model.Subject = entry.Subject;
                            model.Date = entry.StruggleDate;
                            model.Entry = entry.Description;
                            model.Tags = data.Tags.Select(x => new TagDto() { Id = x.Id, TagName = x.TagName, TagType = x.TagType }).ToList();

                            var taskTags = entry.StruggleTags.Select(x => x.TagId).ToList();
                            foreach (var tag in model.Tags)
                            {
                                if (taskTags.Contains(tag.Id))
                                    tag.IsSelected = true;
                            }

                            return model;
                        }
                    case DataType.Beterrments:
                        {
                            var entry = data.Betterments.Find(id);
                            if (entry == null)
                                return new DataDto()
                                {
                                    Tags = data.Tags.Select(x => new TagDto() { Id = x.Id, TagName = x.TagName, TagType = x.TagType }).ToList()
                                };

                            model.Id = entry.Id;
                            model.Subject = entry.Subject;
                            model.Date = entry.BeterrmentDate;
                            model.Entry = entry.Description;
                            model.Tags = data.Tags.Select(x => new TagDto() { Id = x.Id, TagName = x.TagName, TagType = x.TagType }).ToList();

                            var taskTags = entry.BettermentTags.Select(x => x.TagId).ToList();
                            foreach (var tag in model.Tags)
                            {
                                if (taskTags.Contains(tag.Id))
                                    tag.IsSelected = true;
                            }

                            return model;
                        }
                    case DataType.PointOfInterest:
                        {
                            var entry = data.InterestedReads.Find(id);
                            if (entry == null)
                                return new DataDto()
                                {
                                    Tags = data.Tags.Select(x => new TagDto() { Id = x.Id, TagName = x.TagName, TagType = x.TagType }).ToList()
                                };

                            model.Id = entry.Id;
                            model.Date = entry.InterestedReadDate;
                            model.Entry = entry.Description;
                            model.Subject = entry.Subject;
                            model.Tags = data.Tags.Select(x => new TagDto() { Id = x.Id, TagName = x.TagName, TagType = x.TagType }).ToList();

                            var taskTags = entry.InterestedReadTags.Select(x => x.TagId).ToList();
                            foreach (var tag in model.Tags)
                            {
                                if (taskTags.Contains(tag.Id))
                                    tag.IsSelected = true;
                            }

                            return model;
                        }
                    default:
                        return null;
                }
            }
        }

        public static DataStatusDto GetDataStatus(int userId)
        {
            using (var data = new ActivityJournalingEntities())
            {
                var startDate = DateTime.Now.Date.AddDays(-(int)DateTime.Now.DayOfWeek);
                var endDate = DateTime.Now;

                var struggleTags = (from s in data.Struggles
                                    join st in data.StruggleTags on s.Id equals st.StruggleId
                                    join t in data.Tags on st.TagId equals t.Id
                                    where s.UserId == userId && s.StruggleDate >= startDate && s.StruggleDate <= endDate
                                    group t by new { t.Id, t.TagName } into tags
                                    select new
                                    {
                                        Count = tags.Count(),
                                        Name = tags.Key.TagName,
                                    })
                                   .OrderByDescending(x => x.Count)
                                   .ToList();

                var model = new DataStatusDto();

                var tag1 = struggleTags.ElementAtOrDefault(0);
                var tag2 = struggleTags.ElementAtOrDefault(1);
                var tag3 = struggleTags.ElementAtOrDefault(2);

                if (tag1 != null)
                    model.StruggleTags.Add(tag1.Name);

                if (tag2 != null)
                    model.StruggleTags.Add(tag2.Name);

                if (tag3 != null)
                    model.StruggleTags.Add(tag3.Name);

                var bettermentsTags = (from s in data.Betterments
                                       join st in data.BettermentTags on s.Id equals st.BettermentId
                                       join t in data.Tags on st.TagId equals t.Id
                                       where s.UserId == userId && s.BeterrmentDate >= startDate && s.BeterrmentDate <= endDate
                                       group t by new { t.Id, t.TagName } into tags
                                       select new
                                       {
                                           Count = tags.Count(),
                                           Name = tags.Key.TagName,
                                       })
                                       .OrderByDescending(x => x.Count)
                                       .ToList();

                tag1 = struggleTags.ElementAtOrDefault(0);
                tag2 = struggleTags.ElementAtOrDefault(1);
                tag3 = struggleTags.ElementAtOrDefault(2);

                if (tag1 != null)
                    model.BettermentsTags.Add(tag1.Name);

                if (tag2 != null)
                    model.BettermentsTags.Add(tag2.Name);

                if (tag3 != null)
                    model.BettermentsTags.Add(tag3.Name);

                var gratitudeTags = (from s in data.InterestedReads
                                    join st in data.InterestedReadTags on s.Id equals st.InterestedReadId
                                    join t in data.Tags on st.TagId equals t.Id
                                    where s.UserId == userId && s.InterestedReadDate >= startDate && s.InterestedReadDate <= endDate
                                    group t by new { t.Id, t.TagName } into tags
                                    select new
                                    {
                                        Count = tags.Count(),
                                        Name = tags.Key.TagName,
                                    })
                                   .OrderByDescending(x => x.Count)
                                   .ToList();

                tag1 = gratitudeTags.ElementAtOrDefault(0);
                tag2 = gratitudeTags.ElementAtOrDefault(1);
                tag3 = gratitudeTags.ElementAtOrDefault(2);

                if (tag1 != null)
                    model.AreaOfGratitudeTags.Add(tag1.Name);

                if (tag2 != null)
                    model.AreaOfGratitudeTags.Add(tag2.Name);

                if (tag3 != null)
                    model.AreaOfGratitudeTags.Add(tag3.Name);

                return model;
            }
        }
    }
}
