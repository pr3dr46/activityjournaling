﻿using ActivityJournaling.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ActivityJournaling.DataLayer;

namespace ActivityJournaling.Services.Core
{
    public class TaskService
    {
        public static int SaveTask(TaskDto task)
        {
            using (var data = new ActivityJournalingEntities())
            {
                Task dbTask = null;

                if (task.Id > 0)
                    dbTask = data.Tasks.Find(task.Id);
                else
                {
                    dbTask = new Task();
                    dbTask.StatusId = (int)TaskStatuses.Pending;
                    data.Tasks.Add(dbTask);

                    //check if exists
                    if (data.Tasks.Where(x => x.UserId == task.UserId)
                        .Any(x => x.Subject == task.Subject &&
                                x.TaskDate.Value.Day == task.TaskDate.Value.Day &&
                                x.TaskDate.Value.Month == task.TaskDate.Value.Month &&
                                x.TaskDate.Value.Year == task.TaskDate.Value.Year))
                    {
                        throw new Exception("Task with given subject already exists.");
                    }
                }

                dbTask.Subject = task.Subject;
                dbTask.Description = task.Description;
                dbTask.TaskDate = task.TaskDate;
                dbTask.UserId = task.UserId;
                dbTask.CalendarEventIdentifier = task.CalendarEventIdentifier;

                dbTask.Comment = task.Comment;
                dbTask.EmojiState = task.EmojiState;

                data.SaveChanges();

                if (task.Tags != null)
                {
                    dbTask.TaskTags.Clear();

                    foreach (var tag in task.Tags)
                    {
                        var dbTaskTag = new TaskTag();
                        dbTaskTag.TagId = tag.Id;
                        dbTaskTag.TaskId = dbTask.Id;
                        data.TaskTags.Add(dbTaskTag);
                    }
                }

                data.SaveChanges();

                return dbTask.Id;
            }
        }

        public static List<TaskDto> GetTasks(int userId, DateTime date, TaskStatuses status)
        {
            using (var data = new ActivityJournalingEntities())
            {
                var tasks = data.Tasks
                    .Where(x => x.TaskDate.Value.Day == date.Day &&
                                x.TaskDate.Value.Month == date.Month &&
                                x.TaskDate.Value.Year == date.Year &&
                                x.StatusId == (int)status &&
                                x.UserId == userId)
                    .Select(x => new TaskDto()
                    {
                        Id = x.Id,
                        Description = x.Description,
                        Subject = x.Subject,
                        TaskDate = x.TaskDate.Value,
                        UserId = x.UserId.Value,
                        Status = (TaskStatuses)x.StatusId,
                        EmojiState = x.EmojiState,
                        Tags = x.TaskTags.Select(y => new TagDto()
                        {
                            Id = y.Id,
                            TagName = y.Tag.TagName,
                            TagType = y.Tag.TagType
                        })
                        .ToList()
                    });

                return tasks.ToList();
            }
        }

        public static void UpdateStatus(int? taskId, TaskStatuses status)
        {
            using (var data = new ActivityJournalingEntities())
            {
                var dbTask = data.Tasks.Find(taskId);
                if (dbTask != null)
                {
                    dbTask.StatusId = (int)status;
                    data.SaveChanges();
                }
            }
        }

        public static TaskDto GetTask(int userId, int? id)
        {
            using (var data = new ActivityJournalingEntities())
            {
                var dbTask = data.Tasks.Find(id);
                var user = data.AspNetUsers.Find(userId);
               
                if (dbTask == null)
                    return new TaskDto()
                    {
                        Tags = data.Tags.Select(x => new TagDto() { Id = x.Id, TagName = x.TagName, TagType = x.TagType }).ToList(),
                        SyncWithCalendar = user.SyncWithCalendar == true
                    };

                var task = new TaskDto();
                task.Id = dbTask.Id;
                task.Description = dbTask.Description;
                task.Status = (TaskStatuses)dbTask.StatusId;
                task.Subject = dbTask.Subject;
                task.TaskDate = dbTask.TaskDate;
                task.UserId = dbTask.UserId;
                task.SyncWithCalendar = user.SyncWithCalendar == true;
                task.CalendarEventIdentifier = dbTask.CalendarEventIdentifier;
                
                task.EmojiState = dbTask.EmojiState;
                task.Comment = dbTask.Comment;
                task.Tags = data.Tags.Select(x => new TagDto() { Id = x.Id, TagName = x.TagName, TagType = x.TagType }).ToList();
                var taskTags = dbTask.TaskTags.Select(x => x.TagId).ToList();

                foreach (var tag in task.Tags)
                {
                    if (taskTags.Contains(tag.Id))
                        tag.IsSelected = true;
                }

                return task;
            }
        }

        public static void DeleteTask(int? taskId)
        {
            using (var data = new ActivityJournalingEntities())
            {
                var dbTask = data.Tasks.Find(taskId);
                if (dbTask != null)
                {
                    var tags = dbTask.TaskTags.ToList();
                    foreach (var tag in tags)
                        dbTask.TaskTags.Remove(tag);

                    data.Tasks.Remove(dbTask);
                    data.SaveChanges();
                }
            }
        }

        public static TaskCountStatusDto GetTaskStatus(int userId)
        {
            using (var data = new ActivityJournalingEntities())
            {
                var startDate = DateTime.Now.Date.AddDays(-(int)DateTime.Now.DayOfWeek);
                var endDate = DateTime.Now; 

                var completedTasks = data.Tasks
                    .Where(x => x.TaskDate >= startDate && x.TaskDate <= endDate && x.UserId == userId && x.StatusId == (int)TaskStatuses.Done)
                    .Count();

                var pendingTasks = data.Tasks
                  .Where(x => x.TaskDate >= startDate && x.TaskDate <= endDate && x.UserId == userId && x.StatusId == (int)TaskStatuses.Pending)
                  .Count();


                int status = 0;
                if (completedTasks == 0)
                    status = 4;
                else
                    status = (int)Math.Round((decimal)pendingTasks / (decimal)completedTasks, 0);

                if (status > 4)
                    status = 4;

                return new TaskCountStatusDto
                {
                    CompletedTasks = completedTasks,
                    PendingTasks = pendingTasks,
                    Status = status
                };
            }
        }
    }
}
